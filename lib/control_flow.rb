# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  letters = str.chars
  letters.map do |char|
    letters.delete(char) if char == char.downcase
  end
  letters.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    str[str.length / 2]
  else
    str[(str.length / 2) - 1] + str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  letters = str.chars
  count = 0
  letters.map do |char|
    count += 1 if VOWELS.include?(char)
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  total = num
  idx = num - 1
  if num == 1
    1
  else
    while idx > 0
      total *= idx
      idx -= 1
    end
  end
  total
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined_string = ""
  arr.map.with_index do |el, i|
    joined_string << el
    joined_string << separator if i != (arr.length - 1)
  end
  joined_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  letters = str.downcase.chars
  letters.map.with_index do |char, i|
    if i.odd?
      letters[i] = char.upcase
    end
  end
  letters.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(" ")
  words.map.with_index do |word, i|
    if word.length >= 5
      words[i] = word.reverse
    end
  end
  words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz_array = []
  (1..n).map do |int|
    if int % 3 == 0 && int % 5 == 0
      fizzbuzz_array << "fizzbuzz"
    elsif int % 3 == 0
      fizzbuzz_array << "fizz"
    elsif int % 5 == 0
      fizzbuzz_array << "buzz"
    else
      fizzbuzz_array << int
    end
  end
  fizzbuzz_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed_array = []
  i = arr.length - 1
  while i > 0
    reversed_array << arr[i]
    i -= 1
  end
  reversed_array
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)

end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)

end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)

end
